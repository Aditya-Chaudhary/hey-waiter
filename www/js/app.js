// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
      .state('intro', {
        url: '/',
        templateUrl: 'views/intro.html'
      })
      .state('login', {
          url: '/login',
          templateUrl: 'views/login.html'
      })
      .state('signup', {
          url: '/signup',
          templateUrl: 'views/signup.html'
      })
      .state('home', {
          url: '/home',
          templateUrl: 'views/home.html'
      })
      .state('dishes', {
          url: '/restaurant/:id',
          templateUrl: 'views/dishes.html'
      });

  $urlRouterProvider.otherwise('/');
});


app.controller('ContentController', function($scope, $ionicSideMenuDelegate) {
    $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
});



app.controller('RestaurantsListController',[ '$scope', function($scope){
    $scope.RestrauntsData =  [
            {
                id: 1,
                name: 'Kulcha King',
                price: '100'
            },
            {
                id: 2,
                name: 'Sagar Ratna - The Ashok',
                price: '200'
            },
            {
                id: 3,
                name: 'Mexitup',
                price: '320'
            },
            {
                id: 4,
                name: 'Hokey Pokey',
                price: '90'
            },
            {
                id: 5,
                name: 'Balaji\'s Vegetarian Paradise',
                price: '250'
            },
            {
                id: 6,
                name: 'The Backyard',
                price: '150'
            },
            {
                id: 7,
                name: 'The Great Kabab Factory',
                price: '85'
            },
            {
                id: 8,
                name: 'Al Jawahar',
                price: '340'
            },
            {
                id: 9,
                name: 'Kulcha King',
                price: '120'
            },
            {
                id: 10,
                name: 'Sagar Ratna - The Ashok',
                price: '118'
            },
            {
                id: 11,
                name: 'Mexitup',
                price: '112'
            },
            {
                id: 12,
                name: 'Hokey Pokey',
                price: '120'
            },
            {
                id: 13,
                name: 'Balaji\'s Vegetarian Paradise',
                price: '70'
            },
            {
                id: 14,
                name: 'The Backyard',
                price: '90'
            },
            {
                id: 15,
                name: 'The Great Kabab Factory',
                price: '200'
            },
            {
                id: 16,
                name: 'Al Jawahar',
                price: '300'
            }
        ];
}]);



app.controller('DishesListController', function($scope, $ionicSideMenuDelegate) {
    $scope.RestrauntsData = [];
});